package info.tsyklop.searcher.resolver;

import info.tsyklop.searcher.result.Result;
import info.tsyklop.searcher.result.item.ResultItem;

import java.io.File;
import java.util.List;
import java.util.Map;

public class ViewResolver {

    public String resolve(Result result, String text4search) {

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<File, List<ResultItem>> entry : result.getMatches().entrySet()) {
            if (entry.getValue().isEmpty()) {
                sb.append(String.format("Wanted text '%s' not found in '%s':", text4search, entry.getKey())).append("\n");
            } else {
                sb.append(String.format("Found %s matches for wanted text '%s' in '%s':", entry.getValue().size(), text4search, entry.getKey())).append("\n");
                for (ResultItem resultItem : entry.getValue()) {
                    sb.append("\t")
                            .append(resultItem.toString())
                            .append("\n");
                }
            }
        }

        return sb.toString();

    }

}
