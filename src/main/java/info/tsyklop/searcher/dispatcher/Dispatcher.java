package info.tsyklop.searcher.dispatcher;

import info.tsyklop.searcher.config.Config;
import info.tsyklop.searcher.exception.DispatchException;
import info.tsyklop.searcher.handler.Handler;
import info.tsyklop.searcher.resolver.ViewResolver;
import info.tsyklop.searcher.result.Result;
import info.tsyklop.searcher.view.View;
import info.tsyklop.searcher.view.impl.ConsoleView;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Dispatcher {

    private final Config config;

    private final Handler handler;

    private final List<View> views;

    private final ViewResolver viewResolver;

    public Dispatcher(String[] args) {

        this.views = new ArrayList<>();
        this.handler = new Handler();
        this.config = new Config(args);
        this.viewResolver = new ViewResolver();

        this.views.add(new ConsoleView());

    }

    public void dispatch() {

        Result result = new Result();

        if (this.config.getDestination().isFile()) {
            result.addMatches(this.config.getDestination(),
                    this.handler.handle(this.config.getDestination(), this.config.getText4Search()));
        } else if (this.config.getDestination().isDirectory()) {
            try {
                Files.find(
                        this.config.getDestination().toPath(),
                        Integer.MAX_VALUE,
                        (path, attributes) -> attributes.isRegularFile()
                ).forEach(path -> {
                    result.addMatches(path.toFile(),
                            this.handler.handle(path.toFile(), this.config.getText4Search()));
                });
            } catch (IOException e) {
                throw new DispatchException("Error finding files in destination", e);
            }
        } else {
            throw new DispatchException("Destination is not file or directory");
        }

        String textResult = this.viewResolver.resolve(result, this.config.getText4Search());
        for (View view : this.views) {
            view.print(textResult);
        }

    }

}
