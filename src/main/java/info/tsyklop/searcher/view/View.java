package info.tsyklop.searcher.view;

public interface View {

    void print(String message);

}
