package info.tsyklop.searcher.view.impl;

import info.tsyklop.searcher.view.View;

public class ConsoleView implements View {

    @Override
    public void print(String message) {
        if(message != null && !message.isEmpty()) {
            System.out.println(message);
        }
    }

}
