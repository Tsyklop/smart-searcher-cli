package info.tsyklop.searcher.result.item.impl;

import info.tsyklop.searcher.result.item.ResultItem;

public class SimpleResultItem implements ResultItem {

    private final String line;

    private final int lineNumber;

    public SimpleResultItem(int lineNumber, String line) {
        this.line = line;
        this.lineNumber = lineNumber;
    }

    @Override
    public String getFormatted() {
        return String.format("[%s] -> %s", this.lineNumber, this.line);
    }

}
