package info.tsyklop.searcher.result.item;

public interface ResultItem {

    String getFormatted();

}
