package info.tsyklop.searcher.result.item.impl;

import info.tsyklop.searcher.result.item.ResultItem;

public class XmlResultItem implements ResultItem {

    private final String node;

    public XmlResultItem(String node) {
        this.node = node;
    }

    @Override
    public String getFormatted() {
        return String.format("%s", this.node);
    }

}
