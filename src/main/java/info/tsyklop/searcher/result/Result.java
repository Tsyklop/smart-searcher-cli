package info.tsyklop.searcher.result;

import info.tsyklop.searcher.result.item.ResultItem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Result {

    private final Map<File, List<ResultItem>> matches = new HashMap<>();

    public Map<File, List<ResultItem>> getMatches() {
        return matches;
    }

    public void addMatches(File file, List<ResultItem> resultItems) {
        Objects.requireNonNull(file, "File cannot be null");
        Objects.requireNonNull(resultItems, "Result items cannot be null");
        this.matches.compute(file, (file1, items) -> {
            if (items == null) {
                items = new ArrayList<>();
            }
            items.addAll(resultItems);
            return items;
        });
    }

    public boolean isEmpty() {
        return this.matches.isEmpty();
    }

}
