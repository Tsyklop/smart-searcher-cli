package info.tsyklop.searcher.config;

import info.tsyklop.searcher.exception.ConfigException;

import java.io.File;

public class Config {

    //private File outputFile;

    private File destination;
    private String text4Search;

    /*private boolean useOutputFile;

    private boolean printFullPath;

    private String outputFileExtension;*/

    public Config(String[] args) {
        parseArgs(args);
    }

    public File getDestination() {
        return destination;
    }

    public String getText4Search() {
        return text4Search;
    }

   /* public File getOutputFile() {
        return outputFile;
    }

    public boolean isPrintFullPath() {
        return printFullPath;
    }

    public String getOutputFileExtension() {
        return outputFileExtension;
    }*/

    private void parseArgs(String[] args) {

        if (args.length < 2) {
            throw new ConfigException("Need more arguments");
        }

        this.text4Search = args[args.length - 2];
        this.destination = new File(args[args.length - 1]);

        if (this.text4Search == null || this.text4Search.isEmpty()) {
            throw new ConfigException("Text for search not found");
        }

        if (!this.destination.exists()) {
            throw new ConfigException("Destination not found");
        }

        /*for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            switch (arg) {
                case "-p":
                    this.printFullPath = true;
                    break;
                case "-s":
                    this.outputFileExtension = args[i + 1];
                    i++;
                    break;
                case "-o":
                    this.outputFile = new File(args[i + 1]);
                    i++;
                    break;
                default:
                    break;
            }
        }

        this.printFullPath = this.printFullPath && this.outputFileExtension != null && this.outputFileExtension.equals("xml");
        this.useOutputFile = this.outputFile != null;
*/
    }

}
