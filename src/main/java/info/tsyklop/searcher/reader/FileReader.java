package info.tsyklop.searcher.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class FileReader {

    public List<String> readLines(File file) throws IOException {
        return Files.readAllLines(file.toPath());
    }

}
