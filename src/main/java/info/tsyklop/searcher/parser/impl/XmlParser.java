package info.tsyklop.searcher.parser.impl;

import info.tsyklop.searcher.parser.AbstractParser;
import info.tsyklop.searcher.result.item.ResultItem;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XmlParser extends AbstractParser {

    public XmlParser() {
        super(Arrays.asList("xml", "html"));
    }

    @Override
    public List<ResultItem> parse(File file, String text4Search) {

        List<ResultItem> resultItems = new ArrayList<>();

        try {

            DocumentBuilder docBuilder = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder();
            Document doc = docBuilder.parse(new FileInputStream(file));

            //search(resultItems, text4Search, doc.getChildNodes());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultItems;

    }

    /*private void search(List<ResultItem> resultItems, String textForSearch, NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //System.out.println(node.toString());
                checkNode(resultItems, textForSearch, node);
                if (node.hasAttributes()) {
                    NamedNodeMap namedNodeMap = node.getAttributes();
                    for (int j = 0; j < namedNodeMap.getLength(); j++) {
                        //System.out.println("\tAttribute: " + namedNodeMap.item(j));
                        checkAttributeNode(resultItems, textForSearch, node, (Attr) namedNodeMap.item(j));
                    }
                }
                search(resultItems, textForSearch, node.getChildNodes());
            }

        }
    }*/

    /*@Deprecated
    private void search(List<ResultItem> resultItems, String textForSearch, Node node) {
        System.out.println(node.getNodeName());
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node childNode = children.item(i);
            System.out.println("\t"+childNode.getNodeName());
            checkNode(resultItems, textForSearch, childNode);
            if (childNode.hasAttributes()) {
                NamedNodeMap namedNodeMap = childNode.getAttributes();
                for (int j = 0; j < namedNodeMap.getLength(); j++) {
                    checkNode(resultItems, textForSearch, namedNodeMap.item(j));
                }
            }
            search(resultItems, textForSearch, childNode);
        }
    }*/

    /*private void checkNode(List<ResultItem> resultItems, String text4search, Node node) {

        if (node.getNodeName().contains(text4search)) {
            resultItems.add(new XmlResultItem(node.getNodeName()));
        }

        if (node.getNodeValue() != null && node.getNodeValue().contains(text4search)) {
            resultItems.add(new XmlResultItem(node.getNodeName() + " -> " + node.getNodeValue()));
        }

    }

    private void checkAttributeNode(List<ResultItem> resultItems, String text4search, Node node, Attr attr) {

        if (attr.getNodeName().contains(text4search)) {
            resultItems.add(new XmlResultItem(node.getNodeName()));
        }

        if (attr.getNodeValue() != null && attr.getNodeValue().contains(text4search)) {
            resultItems.add(new XmlResultItem(node.getNodeName() + " -> " + "[" + attr.getName() + "=" + attr.getNodeValue() + "]"))
            ;
        }

    }*/

}
