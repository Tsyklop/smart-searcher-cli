package info.tsyklop.searcher.parser.impl;

import info.tsyklop.searcher.parser.AbstractParser;
import info.tsyklop.searcher.reader.FileReader;
import info.tsyklop.searcher.result.item.ResultItem;
import info.tsyklop.searcher.result.item.impl.SimpleResultItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TxtParser extends AbstractParser {

    private final FileReader fileReader = new FileReader();

    public TxtParser() {
        super(Arrays.asList("txt", "log"));
    }

    @Override
    public List<ResultItem> parse(File file, String text4Search) {

        List<ResultItem> resultItems = new ArrayList<>();

        try {

            List<String> lines = fileReader.readLines(file);

            int lineNumber = 1;
            for (String line : lines) {
                if (line.contains(text4Search)) {
                    resultItems.add(new SimpleResultItem(lineNumber, line));
                }
                lineNumber++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultItems;

    }

}
