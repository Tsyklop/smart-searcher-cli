package info.tsyklop.searcher.parser;

import java.util.List;
import java.util.Objects;

public abstract class AbstractParser implements Parser {

    private final List<String> supportedExtensions;

    public AbstractParser(List<String> supportedExtensions) {
        this.supportedExtensions = Objects.requireNonNull(supportedExtensions);
    }

    @Override
    public List<String> getSupportedExtensions() {
        return this.supportedExtensions;
    }

    @Override
    public boolean isSupportFile(String fileName) {
        return fileName != null && fileName.contains(".")
                && getSupportedExtensions()
                .contains(
                        fileName.substring(fileName.lastIndexOf('.') + 1)
                );
    }

}
