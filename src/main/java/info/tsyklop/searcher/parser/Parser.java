package info.tsyklop.searcher.parser;

import info.tsyklop.searcher.result.item.ResultItem;

import java.io.File;
import java.util.List;

public interface Parser {

    List<String> getSupportedExtensions();

    boolean isSupportFile(String fileName);

    List<ResultItem> parse(File file, String text4Search);

}
