package info.tsyklop.searcher;

import info.tsyklop.searcher.dispatcher.Dispatcher;

public class Main {

    public static void main(String[] args) {
        try {

            System.out.println("==============================================================");
            System.out.println("Smart File Handler CLI");
            System.out.println("==============================================================");

            new Dispatcher(args).dispatch();

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
