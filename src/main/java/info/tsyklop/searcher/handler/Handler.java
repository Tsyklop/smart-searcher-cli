package info.tsyklop.searcher.handler;

import info.tsyklop.searcher.parser.Parser;
import info.tsyklop.searcher.parser.impl.JsonParser;
import info.tsyklop.searcher.parser.impl.TxtParser;
import info.tsyklop.searcher.parser.impl.XmlParser;
import info.tsyklop.searcher.result.item.ResultItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Handler {

    private final List<Parser> parsers = new ArrayList<>();

    public Handler() {
        this.parsers.add(new TxtParser());
        this.parsers.add(new XmlParser());
        this.parsers.add(new JsonParser());
    }

    public List<ResultItem> handle(File file, String text4search) {
        List<ResultItem> items = new ArrayList<>();
        for (Parser parser : this.parsers) {
            if (parser.isSupportFile(file.getName())) {
                items.addAll(parser.parse(file, text4search));
            }
        }
        return items;
    }

}
