# smart-searcher-cli

Main features:

- run from command line 
    `java -jar smart-searcher-cli.jar [-p [-s txt|xml]] [-o <file>] "text for search" <dir|file>`
- acceptable file extensions: `.txt`, `.xml`, `.json` and `.html` 
- produce:
  - output message with count of founded text for search
  - for `.txt` output lines from files with text for search
  - for `.xml` or `.html` output tags files with text for search
  - `-s txt|xml`  save output messages in file with selected extension
  - `-p`  for `.xml` or `.html` output message contains full path tag
  - `-o <file>` path to file with result
  - `<dir|file>` source dir or file
